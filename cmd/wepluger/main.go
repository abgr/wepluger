// +build freebsd linux darwin

// Wepluger       Go plugins web server

// DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
// Version 2, December 2004
//
// Copyright (C) 2020 ABGR
//
// Everyone is permitted to copy and distribute verbatim or modified
// copies of this license document, and changing it is allowed as long
// as the name is changed.
//
// DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
// TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
//
// 0. You just DO WHAT THE FUCK YOU WANT TO.

// VIVA FREEDOM

package main

import (
	"flag"

	wepluger "gitea.com/abgr/wepluger"
)

func main() {
	Port := flag.Uint("p", 8080, "server port")
	Log := flag.String("l", ":console:", "log output")
	Directory := flag.String("d", ".", "plugins directory")
	Cert := flag.String("c", "", "public cert path")
	Key := flag.String("k", "", "private key path")
	flag.Parse()
	wepluger.Start(*Port, *Log, *Directory, *Cert, *Key)
}
