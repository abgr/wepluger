//+build freebsd linux darwin

// Wepluger       Go plugins web server

// DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
// Version 2, December 2004
//
// Copyright (C) 2020 ABGR
//
// Everyone is permitted to copy and distribute verbatim or modified
// copies of this license document, and changing it is allowed as long
// as the name is changed.
//
// DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
// TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
//
// 0. You just DO WHAT THE FUCK YOU WANT TO.

// VIVA FREEDOM

package wepluger

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"plugin"
	"strconv"
	"syscall"
	"time"
	"unicode"

	"gitea.com/abgr/aerr"
	"github.com/egymgmbh/go-prefix-writer/prefixer"
	"github.com/karrick/godirwalk"
	"github.com/valyala/fasthttp"
)

var logger *log.Logger
var logFile *os.File

var httpServer *fasthttp.Server

//loadedPlugin  describes a open and used webplug plugin
type loadedPlugin struct {
	plugin    *plugin.Plugin
	OnRequest func(*fasthttp.RequestCtx, interface{})
}

//openPlugin Opens a webplug plugin by its directory path
func openPlugin(dir string) (p loadedPlugin, e error) {
	logAnyway("Open:", dir)
	e = runScripts(dir)
	if e != nil {
		return
	}
	plug, e := plugin.Open(filepath.Join(dir, ".so"))
	if e != nil {
		return
	}
	symb, e := plug.Lookup("OnRequest")
	if e != nil {
		return
	}
	onRequest, ok := symb.(func(*fasthttp.RequestCtx, interface{}))
	if !ok {
		e = errors.New("invalid plugin")
		return
	}
	p.OnRequest = onRequest
	p.plugin = plug
	return

}

//runScripts scans and runs all webplug plugin included scripts
func runScripts(dir string) (e error) {
	initShPath := filepath.Join(dir, "wepluger.init")
	is, e := isRegularFile(initShPath)
	if e != nil {
		return e
	}
	if is {
		e = runShellScript(initShPath)
		if e != nil {
			return e
		}
	}
	return
}

//runShellScript  runs a bash scripts
func runShellScript(path string) error {
	logAnyway("Run:", path)
	cmd := exec.Command("sh", filepath.Base(path))
	cmd.Dir = filepath.Dir(path)
	cmd.Stderr = prefixer.New(os.Stderr, func() string {
		return "Error:\t"
	})
	cmd.Stdout = prefixer.New(os.Stdout, func() string {
		return "\t"
	})
	e := cmd.Run()
	if e != nil {
		return e
	}
	return nil
}

//isRegularFile checks a file path is a regular file nor socket,device,link etc.
func isRegularFile(path string) (bool, error) {
	info, e := os.Lstat(path)
	if e == nil {
		if info.Mode().IsRegular() {
			return true, nil
		}
		return false, nil
	}
	if os.IsNotExist(e) {
		return false, nil
	}
	return false, e
}

//onPanic defines program behavior on panics
func onPanic(rec interface{}) {
	if err, ok := rec.(error); ok {
		if logger == nil {
			os.Stderr.Write([]byte("Error: " + err.(error).Error() + "\n"))

		} else {
			logger.Println("Error:", err.(error).Error())
		}
		os.Exit(1)
	}
	logger.Println(rec)
	httpServer.Shutdown()
}

//listenSignals listen and do related works on os signals
func listenSignals() {

	sigs := make(chan os.Signal, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	sig := <-sigs
	logAnyway("Signal:", sig.String())
	httpServer.Shutdown()
}

//logAnyway detect output and write logs

func logAnyway(strs ...interface{}) {
	if logger == nil {
		_, e := fmt.Println(strs...)
		aerr.Panicerr(e, nil)
	} else {
		logger.Println(strs...)
	}
}

//getPlugNameFromPath retriev pointed plugin name from http path
func getPlugNameFromPath(path []byte) string {
	if len(path) <= len([]byte("/")) {
		return "/"
	}
	fS := bytes.Index(path, []byte("/"))
	if fS < 0 {
		return "/"
	}
	fS += len([]byte("/"))
	sS := bytes.Index(path[fS+1:], []byte("/"))
	if sS < 0 {
		return string(path[fS:])
	}
	sS += fS + 1
	return string(path[fS:sS])
}
func errorHandler(ctx *fasthttp.RequestCtx, err error) {
	if ctx.Err() != nil {
		return
	}
	plug, found := retPlugin(ctx)
	if !found {
		return
	}
	var rec interface{}
	fn := func() {
		plug.OnRequest(ctx, rec)
	}
again:
	rec = aerr.Guard(fn)
	if rec != nil && ctx.Err() == nil {
		goto again
	}
}

func handler(ctx *fasthttp.RequestCtx) {
	plug, found := retPlugin(ctx)
	if !found {
		ctx.SetStatusCode(404)
		return
	}
	var rec interface{}
	fn := func() {
		plug.OnRequest(ctx, rec)
	}
again:
	rec = aerr.Guard(fn)
	if rec != nil && ctx.Err() == nil {
		goto again
	}

}
func (plug loadedPlugin) SafeOnRequest(ctx *fasthttp.RequestCtx, rec interface{}) interface{} {
	return aerr.Guard(func() {
		plug.OnRequest(ctx, rec)
	})
}
func retPlugin(ctx *fasthttp.RequestCtx) (plug loadedPlugin, found bool) {
	plug, found = plugins[getPlugNameFromPath(ctx.Path())]
	if !found {
		plug, found = plugins["/"]
		return
	}
	return
}

var plugins = make(map[string]loadedPlugin)

//Start starts a wepluger server
func Start(Port uint, Log, Directory, Cert, Key string) {
	go listenSignals()
	var e error
	defer aerr.Catch(onPanic)
	aerr.Assert(Port != 0, aerr.New("invalid port"))
	defer logFile.Close()
	switch Log {
	case ":console:":
		logger = nil
	case "off":
		logger = log.New(ioutil.Discard, "", 0)
	default:
		logFile, e = os.OpenFile(Log, os.O_CREATE|os.O_APPEND, 660)
		aerr.Panicerr(e, nil)
		logger = log.New(logFile, "", 0)
	}
	if Directory == "." {
		dir, e := os.Getwd()
		aerr.Panicerr(e, nil)
		Directory = dir
	}
	aerr.Assert(filepath.IsAbs(Directory), aerr.New("the plugin path must be absolute"))
	logAnyway("Scan:", Directory)
	walker, e := godirwalk.NewScanner(Directory)
	aerr.Panicerr(e, nil)
	//
	plug, e := openPlugin(Directory)
	aerr.Panicerr(e, nil)
	plugins["/"] = plug
	//
dirWalker:
	for walker.Scan() {
		dirent, e := walker.Dirent()
		aerr.Panicerr(e, nil)
		for _, rune0 := range dirent.Name() {
			if unicode.IsLetter(rune0) && unicode.IsUpper(rune0) {
				break
			}
			continue dirWalker
		}
		if is, e := dirent.IsDirOrSymlinkToDir(); is == false {
			aerr.Panicerr(e, nil)
			continue
		}
		dirPath := filepath.Join(Directory, dirent.Name())
		if dirent.IsSymlink() {
			dirPath, e = os.Readlink(dirPath)
			aerr.Panicerr(e, nil)
		}
		plug, e := openPlugin(dirPath)
		aerr.Panicerr(e, nil)
		plugins[dirent.Name()] = plug
	}
	//configure the web server
	httpServer = &fasthttp.Server{
		Name:                               "wepluger",
		NoDefaultDate:                      true,
		SleepWhenConcurrencyLimitsExceeded: time.Second * 1,
		NoDefaultContentType:               true,
		DisableHeaderNamesNormalizing:      true,
		DisablePreParseMultipartForm:       true,
		ReduceMemoryUsage:                  false,
		ErrorHandler:                       errorHandler,
		IdleTimeout:                        time.Second * 10,
		Handler:                            handler,
		LogAllErrors:                       false,
		Logger:                             fasthttpDiscardedLogger{},
		// HeaderReceived:                     headerReceived,
	}
	serverAddr := "0.0.0.0:" + strconv.FormatUint(uint64(Port), 10)
	logAnyway("Listen:", serverAddr)
	if len(Key) == 0 {
		aerr.Panicerr(httpServer.ListenAndServe(serverAddr), nil)
	} else {
		aerr.Panicerr(httpServer.ListenAndServeTLS(serverAddr, Cert, Key), nil)
	}
}

type fasthttpDiscardedLogger struct{}

func (fasthttpDiscardedLogger) Printf(_ string, _ ...interface{}) {}
