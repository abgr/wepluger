#!/bin/bash
WEPLUGER_RESERVED_PWD=$PWD
WEPLUGER_BASE_DIR=`dirname $0`
WEPLUGER_BASE_DIR=`dirname $WEPLUGER_BASE_DIR`
cd $WEPLUGER_BASE_DIR &&
cd "$WEPLUGER_BASE_DIR/cmd/wepluger" &&
(go test &&
go build -v -o "$WEPLUGER_BASE_DIR/bin/wepluger" ) &&
cd $WEPLUGER_RESERVED_PWD