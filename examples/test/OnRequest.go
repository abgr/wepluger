package main

import (
	"github.com/valyala/fasthttp"
)

func OnRequest(ctx *fasthttp.RequestCtx, rec interface{}) {
	if rec != nil {
		if err, ok := rec.(error); ok {
			ctx.WriteString(err.Error())
		}
		ctx.WriteString("unknown error")
		return
	}
	ctx.WriteString("hello wepluger!")
}
