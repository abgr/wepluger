package utils

import (
	"gitea.com/abgr/wepluger"
	"github.com/valyala/fasthttp"
)

func HandleCORS(ctx *fasthttp.RequestCtx, origin string) {
	if ctx.IsOptions() == false {
		return
	}
	ctx.Response.Header.Set("Access-Control-Max-Age", "86400")
	ctx.Response.Header.Set("Access-Control-Allow-Headers", "*")
	ctx.Response.Header.Set("Access-Control-Allow-Methods", "GET,HEAD,POST")
	ctx.Response.Header.Set("Access-Control-Allow-Origin", origin)
	wepluger.BreakRequest()
}
